# Format Projects

<h3>format all .java files of a project with the google-java-format https://github.com/google/google-java-format</h3>

<p>format at pre-commit hook: https://gitlab.com/Mbg99/format-at-pre-commit-hook</p>
<p>install the pre-commit hook: https://gitlab.com/Mbg99/pre-commit-hook-install</p>


<p>me: https://github.com/Mbg999</p>
